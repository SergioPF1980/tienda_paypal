<?php
    require '../config/config.php';

    if(isset($_POST['id'])){
        $id = $_POST['id'];
        $token = $_POST['token'];

        $token_tmp = hash_hmac('sha1', $id, KEY_TOKEN);
            
        if($token == $token_tmp){

            if(isset($_SESSION['carrito']['productos'][$id])){
                $_SESSION['carrito']['productos'][$id] += 1;
            }else{
                $_SESSION['carrito']['productos'][$id] = 1;
            }
            $dates['number'] = count($_SESSION['carrito']['productos']);
            $dates['ok'] = true;
        }else{
            $dates['ok'] = false;
        }
    }else{
        $dates['ok'] = false;
    }

    echo json_encode($dates);