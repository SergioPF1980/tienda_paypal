<table class="table">
                    <thead>
                        <tr>
                            <th>Productos</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Subtotal</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($lista_carrito == null){
                                echo '<tr><td colspan="5" class="text-center"><b>Lista vacia</b></td></tr>';
                            }else{
                                $total = 0;
                                foreach($lista_carrito as $producto){ 
                                    $_id = $producto['id'];
                                    $nombre = $producto['nombre'];
                                    $precio = $producto['precio'];
                                    $descuento = $producto['descuento'];
                                    $precio_desc = $precio -(($precio * $descuento) / 100);
                                    $subtotal = $cantidad * $precio_desc;

                                    $total += $subtotal;
                                    ?>
                        <tr>
                            <td><?php echo $nombre; ?></td>
                            <td><?php echo CURRENCY . number_format($precio_desc, 2, '.', '.');?></td>
                            <td>
                                <input type="number" name="" id="cantidad_<?php echo $_id; ?>" min="1" step="1" value="<?php $cantidad ?>" size="5" onchange="">
                            </td>
                            <td>
                                <div id="subtotal_<?php echo $_id ?>" name="subtotal[]">
                                    <?php echo CURRENCY . number_format($subtotal, 2, '.', '.'); ?>
                                </div>
                            </td>
                            <td><a href="#" id="eliminar" class="btn btn-warning btn-sm" data-bs-id="<?php echo $_id; ?>" data-bs-toogle="modal" data-bs-target="eliminaModal">Eliminar</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <?php } ?>
                </table>