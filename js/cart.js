function addProducto(id, token) {
    let url = 'classes/carrito.php';
    let formData = new FormData();
    formData.append('id', id);
    formData.append('token', token);

    fetch(url, {
        method: 'POST',
        body: formData,
        mode: 'cors'
    }).then(response => response.json())
    .then(data => {
        if(data.ok){
            let element = document.getElementById('num_cart');
            element.innerHTML = data.number;
        }
    });
}